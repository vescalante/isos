installation-steps.txt: Step by step installation instructions with some notes/solutions to issues I encountered during the installation

threesteps.text: three next steps I'd like to take to improve the process and identify a tool that could be used for at least one of the next steps.

automation: 
----1. start_stop_confluence: start/stops confluence as another user (specified by user input)

Downloads: 
----1. screenshots.zip: Running application with some basic customizations
----2. Install Confluence and PostgreSQL.pdf: Same as installation-steps.txt, but in pdf form
----3. confluence.mov: Recorded interaction with the confluence instance